
import styled from '@emotion/styled';
import { Paper, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow } from '@mui/material'


const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: '#4682b4',
    color: 'white',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#fafafa',
  },
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const UserList = ({users}) => {
  return (
    
    <TableContainer sx={{ width: 'stretch', marginLeft: '5px', marginRight: '5px' }} component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead key={Math.random()} >
          <TableRow key={Math.random()} >
            <StyledTableCell align="left">Username</StyledTableCell>
            <StyledTableCell align="left">Name</StyledTableCell>
            <StyledTableCell align="left">Repos</StyledTableCell>
            <StyledTableCell align="left">Gists</StyledTableCell>
            <StyledTableCell align="left">Followers</StyledTableCell>
            <StyledTableCell align="left">Following</StyledTableCell>
            <StyledTableCell align="left">Created</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => (
            <StyledTableRow
              key={user.login}>
              <StyledTableCell align="left" ><a href={user.url}>{user.login}</a></StyledTableCell>
              <StyledTableCell align="left">{user.name}</StyledTableCell>
              <StyledTableCell align="left">{user.repos}</StyledTableCell>
              <StyledTableCell align="left">{user.gists}</StyledTableCell>
              <StyledTableCell align="left">{user.followers}</StyledTableCell>
              <StyledTableCell align="left">{user.following}</StyledTableCell>
              <StyledTableCell align="left">{user.created}</StyledTableCell>
            </StyledTableRow>
                        
                        
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default UserList   