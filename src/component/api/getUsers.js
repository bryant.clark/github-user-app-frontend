export const fetchUsers = async() => {
    try {
    const responce = await fetch(`http://localhost:5000/users`)
    const json = await responce.json()
    const userArray = Object.keys(json).map((key) => json[key]);
    return userArray 
    }
    catch(error) {
      return error
    }

} 