import axios from 'axios'

export const postUsers = async(input) => {
    const response = await axios({
        method: 'post',
        url: 'http://localhost:5000/users',
        data: {input
        }
      })
    return response.data
} 