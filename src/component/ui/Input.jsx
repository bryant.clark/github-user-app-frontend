import { TextField } from '@mui/material'
import { useState } from 'react'

const NameInput = (props) => {

    const [name, setName] = useState()
    const [isEmpty, setIsEmpty] = useState(false)

    const submitHandler = (event) => {
        setIsEmpty(false)

        if (!name) {
         setIsEmpty(true)
        }
        else {
            props.onEntered(name.toLowerCase())
        }
    }

    return (
        <>
        {!isEmpty&&<TextField  
            size='small' 
            sx={{ width: 'stretch', margin: '6px' }} 
            id="outlined-basic" 
            label="Enter Name" 
            variant="outlined" 
            type='text' required 
            onChange={e => setName(e.target.value)} 
            onKeyPress={(event) => {if (event.key === "Enter") submitHandler()}} 
        />}
        {isEmpty&&<TextField  
            error size='small' 
            sx={{ width: "stretch", margin: '6px' }} 
            id="outlined-basic" 
            label="Enter Name" 
            helperText='Text field empty.' 
            variant="outlined" 
            type='text' required 
            onChange={e => setName(e.target.value)}  
            onKeyPress={(event) => {if (event.key === "Enter") submitHandler()}}
        />}
        </>
    )
}

export default NameInput 