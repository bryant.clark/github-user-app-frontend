import {postUsers} from "../api/postUsers"
import NameInput from "../ui/Input"
import UserList from "../users/UserList"
import { useState, useEffect } from "react"
import { Alert } from "@mui/material"
import { fetchUsers } from "../api/getUsers"

export const HomePage = () => {
    const [isError, setIsError] = useState(false)
    const [apiMessage, setApiMessage] = useState()
    const [isLoading, setIsLoading] = useState(false)
    const [loadedUsers, setLoadedUsers] = useState()

    
    const getUsers = async() => { 
        const res = await fetchUsers()
        setLoadedUsers(res)
    }
    useEffect(() => {getUsers()}, [])

    const enteredHandler = async(enteredName) => {
        setIsLoading(true)
        try {
            const response =  await postUsers(enteredName)
            setIsLoading(false)
            if (response==='User saved.') {
                setIsError(false)
                setApiMessage(response)
                getUsers()
                setTimeout(
                    () => {
                        setApiMessage('')
                    },
                    1500)
            }
            else {
                setIsError(true)
                getUsers()
                setApiMessage(response)
                    setIsError(true)
                    setTimeout(
                        () => {
                            setApiMessage('')
                        },
                        1700)
            }
        } catch (error) {
            return error
    }
}
    return ( <>
    <NameInput onEntered={enteredHandler}/>
    {isLoading&&<div>Loading...</div>}
    {apiMessage&&<Alert severity={isError? 'error' : 'success'}>{apiMessage}</Alert>}
    {loadedUsers&&<UserList users={loadedUsers} />}
    </>
    )
}
