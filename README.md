# Using this app

This GitHub user app frontend from (https://gitlab.com/bryant.clark/github-user-app-frontend) must be used in conjunction with the my github user app backend from (https://gitlab.com/bryant.clark/github-user-app)

# Getting started

Download this app as well as the backend app from above and open them in your IDE.

For best results start the server in the Backend app first. Instructions on how are in its README.md

Once you have the backend running return to this project and in your terminal run `cd code-chalenge` then `npm i` and `npm start`.